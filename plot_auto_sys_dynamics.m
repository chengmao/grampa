% Plot performance of GRAMPA on autonomous system networks from the Oregon dataset
clear;

%% plot number of matched vertices
load('.\mat_files\auto_sys_dynamics.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};
vec_date = ['03/31'; '04/07'; '04/14'; '04/21'; '04/28'; '05/05'; '05/12'; '05/19'; '05/26'];

figure;
plot(corr_sp, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(corr_dp, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size); 

set(gca, 'xtick', [1:9], 'xticklabel', vec_date);
legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Date', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
% figure_spec = gcf;

print('.\pdf_files\auto_sys_dynamics_corr', '-dpdf'); 

%% plot objective value
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}', '\texttt{Truth}'};

figure;
plot(val_sp/2, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(val_dp/2, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(val_truth/2, '-.*', 'LineWidth', line_width, 'MarkerSize', marker_size);

ylim([8000 25000])
set(gca, 'xtick', [1:9], 'xticklabel', vec_date);
legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Date', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Number of common edges', 'FontSize', 15, 'Interpreter', 'latex');

print('.\pdf_files\auto_sys_dynamics_val', '-dpdf'); 