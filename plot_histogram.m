% plot diagonal and off-diagonal entries of GRAMPA similarity matrix
% with histogram and heatmap

clear;
n=200; m = 50; 
A=randn(n,n)/sqrt(n);A=(A+A')/2;
Z=randn(n,n)/sqrt(n);Z=(Z+Z')/2;
sigma=0.05;
B=A+sigma*Z;

%% compute spectral similarity matrix X
eta=sigma/5;
[U, Lambda] = eig(A);
[V, Mu] = eig(B);
lambda = diag(Lambda);
mu = diag(Mu);
Coeff = 1 ./ ((lambda - mu').^2 + eta^2);
Coeff = Coeff .* (U' * ones(n) * V);
X = U * Coeff * V';

%% histogram
figure;
histogram(diag(X), 'FaceColor', [0 0.4470 0.7410], 'EdgeColor', [0 0.4470 0.7410]);
hold on;
h = histfit(nonzeros(X-diag(diag(X))));
h(1).FaceColor = [0.9290 0.6940 0.1250];
h(1).EdgeColor = [0.9290 0.6940 0.1250];
h(2).Color = [0.8500 0.3250 0.0980];

print('.\pdf_files\histogram', '-dpdf'); 

%% heatmap
figure;
colormap summer;
imagesc(X);

colorbar;

print('.\pdf_files\heatmap', '-dpdf'); 