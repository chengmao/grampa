% compare GRAMPA and Umeyama's method in terms of rate of exact recovery
clear;
addpath('.\algorithms');
addpath('.\models');

%% initialization
num_run = 100;
alpha = -0.25;
vec_dim = [400 800 1600];
vec_const = 0:0.05:1;
len_dim = length(vec_dim);
len_const = length(vec_const);

umeyama_rate = zeros(len_dim, len_const);
robust_rate = zeros(len_dim, len_const);

%% Iteration over independent samples 
for ind_run = 1:num_run
    fprintf('Iteration %i \n', ind_run);
    tic;
    
    %% Iteration over dimension
    for ind_dim = 1:len_dim
        n = vec_dim(ind_dim); disp(n);
		
		%% Iteration over noise exponent
        for ind_const = 1:len_const
            sigma = vec_const(ind_const) * n^alpha; 
            [A, B, ~, ~, P_rnd] = generate_er(n, 0.5, sigma);

            %% Umeyama's 
            if vec_const(ind_const) < 0.25
                umeyama_rate(ind_dim, ind_const) = umeyama_rate(ind_dim, ind_const) + 1;
            elseif vec_const(ind_const) < 0.5
                P = matching_umeyama(A, B);
                fix_pt_ratio = sum(dot(P_rnd, P)) / n;
                umeyama_rate(ind_dim, ind_const) = umeyama_rate(ind_dim, ind_const) + (fix_pt_ratio > 0.99999);
            end
            
            %% GRAMPA
            if vec_const(ind_const) < 0.6
                robust_rate(ind_dim, ind_const) = robust_rate(ind_dim, ind_const) + 1;
            elseif vec_const(ind_const) < 1
                P = matching_grampa(A, B, 0.2);
                fix_pt_ratio = sum(dot(P_rnd, P)) / n;
                robust_rate(ind_dim, ind_const) = robust_rate(ind_dim, ind_const) + (fix_pt_ratio > 0.99999);
            end
        end
    end
    toc;
end

umeyama_rate = umeyama_rate/num_run;
robust_rate = robust_rate/num_run;
save('.\mat_files\comparison_sp_umeyama.mat');