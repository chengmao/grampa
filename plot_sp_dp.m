%% plot comparison of GRAMPA and Degree Profile (rate of exact recovery)
clear;
load('.\mat_files\comparison_sp_dp.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{G\,400}', '\texttt{G\,800}', '\texttt{G\,1600}', '\texttt{D\,400}', '\texttt{D\,800}', '\texttt{D\,1600}'};
colors1 = {'#42d4f4', '#0072BD', '#000060'};
colors2 = {'#fabebe', '#D95319', '#800000'};

figure;
for i = 1:len_dim
    plot(vec_const, robust_rate(i, :), '-o', 'Color', colors1{i}, 'LineWidth', line_width);
    hold on;
end
for i = 1:len_dim
    plot(vec_const, deg_pro_rate(i, :), '--+', 'Color', colors2{i}, 'LineWidth', line_width);
    hold on;
end

xlim([0.35 1.25]);
legend(leng_spec, 'location', 'northeast', 'FontSize', 12, 'Interpreter', 'latex');
xlabel('Constant $C$ in $\sigma = C / \log n$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Rate of exact recovery', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp', '-dpdf'); 