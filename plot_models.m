%% plot comparison of performance of GRAMPA on various models
clear;
load('.\mat_files\comparison_models.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'Gaussian Wigner', 'Dense Erd\H{o}s-R\''enyi', 'Sparse Erd\H{o}s-R\''enyi'};

figure;
plot(vec_noise(1:end), wigner_corr_mean(len_dim, 1:end), '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise(1:end), er_corr_mean(len_dim, 1:end), '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise(1:end), ser_corr_mean(len_dim, 1:end), '-.*', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_models', '-dpdf'); 