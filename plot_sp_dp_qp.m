%% plot comparison of GRAMPA, Degree Profile and full QP
clear;
load('.\mat_files\comparison_sp_dp_qp.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}', '\texttt{QP-DS}'};

figure;
plot(vec_noise(1:end), robust_corr_mean(len_dim, 1:end), '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise(1:end), deg_pro_corr_mean(len_dim, 1:end), '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise(1:end), full_qp_corr_mean(len_dim, 1:end), '-.*', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_qp', '-dpdf'); 