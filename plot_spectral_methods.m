%% plot comparison of spectral methods for graph matching
clear; 
load('.\mat_files\comparison_spectral_methods.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{Umeyama''s}', '\texttt{TopEigenVec}', '\texttt{IsoRank}', '\texttt{EigenAlign}', '\texttt{LowRankAlign}'};

figure;

plot(vec_noise, robust_corr_mean(len_dim, :), '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, umeyama_corr_mean(len_dim, :), '-+', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, topeig_corr_mean(len_dim, :), '-*', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, isorank_corr_mean(len_dim, :), '-x', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, eigalign_corr_mean(len_dim, :), '-s', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, lowrank_corr_mean(len_dim, :), '-d', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_spectral_methods', '-dpdf'); 