% Compare performance of GRAMPA on different models for graph matching 
clear;
addpath('.\algorithms');
addpath('.\models');

%% initialization
num_run = 10;
vec_dim = 1000:10:1000;
len_dim = length(vec_dim);
vec_noise = 0:0.05:0.6;
len_noise = length(vec_noise);

wigner_corr = ones(len_dim, len_noise, num_run);
er_corr = ones(len_dim, len_noise, num_run);
ser_corr = ones(len_dim, len_noise, num_run);

%% Iteration over independent samples 
for ind_run = 1:num_run
    tic;
    fprintf('Iteration %i \n', ind_run);

    %% Iteration over dimensions 
    for ind_dim = 1:len_dim
        n = vec_dim(ind_dim);
        
        %% Iteration over noise levels 
        for ind_noise = 3:len_noise
            sigma = vec_noise(ind_noise);
            
            %% Wigner
            [A, B, P_rnd] = generate_wigner_unnormalized(n, sigma*sqrt(2));
            P = matching_grampa(A, B, 0.2);
            fix_pt_ratio = sum(dot(P_rnd, P)) / n;
            wigner_corr(ind_dim, ind_noise, ind_run) = fix_pt_ratio;
            
            %% Erdos-Renyi
            [A, B, ~, ~, P_rnd] = generate_er(n, 0.5, sigma);
            P = matching_grampa(A, B, 0.2);
            fix_pt_ratio = sum(dot(P_rnd, P)) / n;
            er_corr(ind_dim, ind_noise, ind_run) = fix_pt_ratio;
            
            %% Sparse Erdos-Renyi
            [A, B, ~, ~, P_rnd] = generate_er(n, 10/n, sigma);
            P = matching_grampa(A, B, 0.2);
            fix_pt_ratio = sum(dot(P_rnd, P)) / n;
            ser_corr(ind_dim, ind_noise, ind_run) = fix_pt_ratio;
        end
    end
    toc;
end

wigner_corr_mean = mean(mean(wigner_corr, 3), 1);
er_corr_mean = mean(mean(er_corr, 3), 1);
ser_corr_mean = mean(mean(ser_corr, 3), 1);

clear -regexp _corr$;
save('.\mat_files\comparison_models.mat');