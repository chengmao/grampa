%% plot comparison of GRAMPA vs Umeyama's method
clear;
load('.\mat_files\comparison_sp_umeyama.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{G\,400}', '\texttt{G\,800}', '\texttt{G\,1600}', '\texttt{U\,400}', '\texttt{U\,800}', '\texttt{U\,1600}'};
colors1 = {'#42d4f4', '#0072BD', '#000060'};
colors2 = {'#fabebe', '#D95319', '#800000'};

figure;
for i = 1:len_dim
    plot(vec_const, robust_rate(i, :), '-o', 'Color', colors1{i}, 'LineWidth', line_width, 'MarkerSize', marker_size);
    hold on;
end
for i = 1:len_dim
    plot(vec_const, umeyama_rate(i, :), '--+', 'Color', colors2{i}, 'LineWidth', line_width, 'MarkerSize', marker_size);
    hold on;
end

xlim([0.25 1]);
legend(leng_spec, 'location', 'northeast', 'FontSize', 11, 'Interpreter', 'latex');
xlabel('Constant $C$ in $\sigma = C n^{-0.25}$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Rate of exact recovery', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_umeyama', '-dpdf'); 