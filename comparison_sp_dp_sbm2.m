% Compare GRAMPA and Degree Profile for graph matching 
% on stochastic blockmodels
clear;
addpath('.\algorithms');
addpath('.\models');

%% initialization
n = 1000;
num_run = 10;
vec_noise = 0:0.05:0.5;
len_noise = length(vec_noise);

deg_pro_corr = zeros(len_noise, num_run);
robust_corr = zeros(len_noise, num_run);

%% Iteration over independent samples 
for ind_run = 1:num_run
    fprintf('Iteration %i \n', ind_run);
        
    %% Iteration over noise levels 
    for ind_noise = 1:len_noise
        
        sigma = vec_noise(ind_noise); disp(sigma);      
        [A, B, A0, B0, P_rnd] = generate_blockmodel(n, 0.008, 0.002, sigma);

        %% Degree profile
        P = matching_deg_pro(A0, B0);
        fix_pt_ratio = sum(dot(P_rnd, P)) / n;
        deg_pro_corr(ind_noise, ind_run) = fix_pt_ratio;

        %% GRAMPA
        P = matching_grampa(A, B, 0.2); 
        fix_pt_ratio = sum(dot(P_rnd, P)) / n;
        robust_corr(ind_noise, ind_run) = fix_pt_ratio;
    end
end

deg_pro_corr_mean = mean(deg_pro_corr, 2);
robust_corr_mean = mean(robust_corr, 2);

clear -regexp _corr$;
save('.\mat_files\comparison_sp_dp_sbm2.mat');