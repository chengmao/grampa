% Evaluate and plot performance of GRAMPA on subgraphs of autonomous system
% networks from the Oregon dataset
clear;

m = 1000;
corr_sp_all = zeros(9, 1);
corr_dp_all = zeros(9, 1);
val_sp_all = zeros(9, 1);
val_dp_all = zeros(9, 1);
val_truth_all = zeros(9, 1);

for ind_date = 1:9
    load(strcat('.\mat_files\auto_sys_dynamics_', int2str(ind_date), '.mat'));
    
    B_sp = P_sp' * B * P_sp;
    B_dp = P_dp' * B * P_dp;
    B = P_rnd' * B * P_rnd;
    P_sp = P_sp' * P_rnd;
    P_dp = P_dp' * P_rnd;
    
    corr_sp_all(ind_date) = sum(dot(eye(m), P_sp(1:m, 1:m)))/m;
    corr_dp_all(ind_date) = sum(dot(eye(m), P_dp(1:m, 1:m)))/m;
    val_sp_all(ind_date) = sum(dot(A(1:m, 1:m), B_sp(1:m, 1:m)));
    val_dp_all(ind_date) = sum(dot(A(1:m, 1:m), B_dp(1:m, 1:m)));
    val_truth_all(ind_date) = sum(dot(A(1:m, 1:m), B(1:m, 1:m)));
end

%% plot number of matched vertices
line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};
vec_date = ['03/31'; '04/07'; '04/14'; '04/21'; '04/28'; '05/05'; '05/12'; '05/19'; '05/26'];

figure;
plot(corr_sp_all, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(corr_dp_all, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size); 

set(gca, 'xtick', [1:9], 'xticklabel', vec_date);
legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Date', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');

print('.\pdf_files\auto_sys_subgraph_corr', '-dpdf'); 

%% plot objective value
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}', '\texttt{Truth}'};

figure;
plot(val_sp_all/2, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(val_dp_all/2, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(val_truth_all/2, '-.*', 'LineWidth', line_width, 'MarkerSize', marker_size);

set(gca, 'xtick', [1:9], 'xticklabel', vec_date);
legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Date', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Number of common edges', 'FontSize', 15, 'Interpreter', 'latex');

print('.\pdf_files\auto_sys_subgraph_val', '-dpdf'); 