%% plot comparison of GRAMPA and a constrained variant
clear;
load('.\mat_files\comparison_constraint.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'GRAMPA', 'Tighter QP'};

figure;
plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, constraint_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 14, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_constraint', '-dpdf'); 