﻿GRAph Matching by Pairwise eigen-Alignments (GRAMPA)

This repository contrains scripts and data that generate the figures in the papers "Spectral Graph Matching and Regularized Quadratic Relaxations I & II" (2019) by Zhou Fan, Cheng Mao, Yihong Wu and Jiaming Xu. 

The main GRAMPA algorithm is ".\algorithms\matching_grampa.m".