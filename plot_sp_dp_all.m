% Plot GRAMPA vs Degree Profile comparisons on various graph ensembles

%% Erdos-Renyi 1
clear;
load('.\mat_files\comparison_sp_dp_er1.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};

figure;

plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, deg_pro_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 15, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_er1', '-dpdf'); 


%% Erdos-Renyi 2
clear;
load('.\mat_files\comparison_sp_dp_er2.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};

figure;

plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, deg_pro_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 15, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_er2', '-dpdf'); 


%% Stochastic blockmodel 1
clear;
load('.\mat_files\comparison_sp_dp_sbm1.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};

figure;

plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, deg_pro_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 15, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_sbm1', '-dpdf'); 


%% Stochastic blockmodel 2
clear;
load('.\mat_files\comparison_sp_dp_sbm2.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};

figure;

plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, deg_pro_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 15, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_sbm2', '-dpdf'); 


%% Power law 1
clear;
load('.\mat_files\comparison_sp_dp_pl1.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};

figure;

plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, deg_pro_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 15, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_pl1', '-dpdf'); 


%% Plower law 2
clear;
load('.\mat_files\comparison_sp_dp_pl2.mat');

line_width = 1.5;
marker_size = 8;
leng_spec = {'\texttt{GRAMPA}', '\texttt{DegreeProfile}'};

figure;

plot(vec_noise, robust_corr_mean, '-o', 'LineWidth', line_width, 'MarkerSize', marker_size);
hold on;
plot(vec_noise, deg_pro_corr_mean, '--+', 'LineWidth', line_width, 'MarkerSize', marker_size);

legend(leng_spec, 'location', 'northeast', 'FontSize', 15, 'Interpreter', 'latex');
xlabel('Noise level $\sigma$', 'FontSize', 15, 'Interpreter', 'latex');
ylabel('Fraction of correctly matched vertices', 'FontSize', 15, 'Interpreter', 'latex');
figure_spec = gcf;

print('.\pdf_files\comparison_sp_dp_pl2', '-dpdf'); 