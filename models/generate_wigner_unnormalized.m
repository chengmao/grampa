% generate n*n Gaussian Wigner matrices A and B, random permutation matrix P_rnd
% s.t. B = P_rnd (A + sigma*Z) P_rnd'
function [A, B, P_rnd] = generate_wigner_unnormalized(n, sigma)
    A = randn(n);
    A = (A + A')/sqrt(2*n);
    Z = randn(n);
    Z = (Z + Z')/sqrt(2*n);
    B = A + sigma*Z;

    P_rnd = eye(n);
    P_rnd = P_rnd(:, randperm(n));
    B = P_rnd * B * P_rnd';