% generate n*n power law graphs A0 and B0 with edge density p, 
% correlation approximately 1-sigma^2, and underlying matching P_rnd
% normalize (A0, B0) to obtain (A, B)
function [A, B, A0, B0, P_rnd] = generate_power_law(n, p, sigma)
    s = 1 - sigma^2 * (1-p); 
    G = zeros(n); G(1, 2) = 1; G(2, 1) = 1;
    for i = 3:n
        deg_seq = sum(G(1:i-1, 1:i-1));
        G(i, 1:i-1) = rand(1, i-1) < (deg_seq/sum(deg_seq)*n/2/s*p);
        if sum(G(i, 1:i-1)) == 0
            ind_attach = find(mnrnd(1, deg_seq/sum(deg_seq)));
            G(i, ind_attach) = 1; %#ok<FNDSB>
        end 
        G(1:i-1, i) = G(i, 1:i-1)';
    end
    
    Z1 = rand(n) < s;
    Z1 = tril(Z1, -1) + tril(Z1, -1)';
    Z2 = rand(n) < s;
    Z2 = tril(Z2, -1) + tril(Z2, -1)';
    A0 = G .* Z1;
    B0 = G .* Z2;
    
    for i = 1:n
        if sum(A0(:, i)) == 0
            A0(:, i) = G(:, i);
            A0(i, :) = G(i, :);
        end
        if sum(B0(:, i)) == 0
            B0(:, i) = G(:, i);
            B0(i, :) = G(i, :);
        end
    end

    P_rnd = eye(n);
    P_rnd = P_rnd(:, randperm(n));
    B0 = P_rnd * B0 * P_rnd';

    A = A0 - p;
    B = B0 - p;
    A = A/sqrt(n*p*(1-p));
    B = B/sqrt(n*p*(1-p)); 