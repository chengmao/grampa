% Compare unconstrained and constrained QP (GRAMPA vs tighter)
clear;
addpath('.\algorithms');
addpath('.\models');

%% initialization
n = 1000;
num_run = 10;
vec_noise = 0:0.025:0.5;
len_noise = length(vec_noise);

constraint_corr = ones(len_noise, num_run);
robust_corr = ones(len_noise, num_run);

%% Iteration over independent samples 
for ind_run = 1:num_run
    fprintf('Iteration %i \n', ind_run);
        
    %% Iteration over noise levels 
    for ind_noise = 4:len_noise
        tic;
        
        sigma = vec_noise(ind_noise); disp(sigma);      
        [A, B, A0, B0, P_rnd] = generate_er(n, 0.5, sigma);

        %% Constrained spectral
        P = matching_spectral_one_constraint(A, B, 0.1);
        fix_pt_ratio = sum(dot(P_rnd, P)) / n;
        constraint_corr(ind_noise, ind_run) = fix_pt_ratio;

        %% GRAMPA
        P = matching_grampa(A, B, 0.1); 
        fix_pt_ratio = sum(dot(P_rnd, P)) / n;
        robust_corr(ind_noise, ind_run) = fix_pt_ratio;
        
        toc;
    end
end

constraint_corr_mean = mean(constraint_corr, 2);
robust_corr_mean = mean(robust_corr, 2);

clear -regexp _corr$;
save('.\mat_files\comparison_constraint.mat');