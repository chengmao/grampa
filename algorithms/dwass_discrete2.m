function [w,coupling] = dwass_discrete2(x,y,px,py)
% compute W_1 between discrete distributions \sum px(i)\delta_{x(i)} and \sum px(i)\delta_{x(i)}
% where the cardinality of the support may not be the same
% input: x,y,px,py are column vectors (already sorted in ascending order)
if length(x)==length(px) && length(y)==length(py) 
    
   nx=length(x);ny=length(y);
   
    ind_x=nx;
    ind_y=ny;
    mass_x=px(ind_x);
    mass_y=py(ind_y);
    w=0;
    coupling=zeros(nx,ny);
    while ind_x>0 && ind_y>0
        
        temp=min(mass_x,mass_y);
        coupling(ind_x,ind_y)=coupling(ind_x,ind_y)+temp;
        w=w+abs(x(ind_x)-y(ind_y))*temp;
        mass_x=mass_x-temp;
        mass_y=mass_y-temp; 
        if mass_x==0
            ind_x=ind_x-1;
            if ind_x>=1
                mass_x=px(ind_x);
            end
        end     
        if mass_y==0
            ind_y=ind_y-1;
            if ind_y>=1
               mass_y=py(ind_y);
            end
        end
    end
    
else
    error('length not equal')
end
    