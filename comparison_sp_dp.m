% compare GRAMPA and Degree Profile in terms of rate of exact recovery
clear;
addpath('.\algorithms');
addpath('.\models');

%% initialization
num_run = 100;
vec_dim = [400 800 1600];
vec_const = 0.35:0.05:1.25;
len_dim = length(vec_dim);
len_const = length(vec_const);

deg_pro_rate = zeros(len_dim, len_const);
robust_rate = zeros(len_dim, len_const);

%% Iteration over independent samples 
for ind_run = 1:num_run
    fprintf('Iteration %i \n', ind_run);
    tic;
    
    %% Iteration over dimension
    for ind_dim = 1:len_dim
        n = vec_dim(ind_dim); disp(n);
		
		%% Iteration over noise exponent
        for ind_const = 1:len_const
            sigma = vec_const(ind_const) / log(n); 
            [A, B, A0, B0, P_rnd] = generate_er(n, 0.5, sigma);
			
            %% Degree profile
            if vec_const(ind_const) < 0.9
                P = matching_deg_pro(A0, B0);
                fix_pt_ratio = sum(dot(P_rnd, P)) / n;
                deg_pro_rate(ind_dim, ind_const) = deg_pro_rate(ind_dim, ind_const) + (fix_pt_ratio > 0.99999);
            end
            
            %% GRAMPA
            if vec_const(ind_const) < 0.65
                robust_rate(ind_dim, ind_const) = robust_rate(ind_dim, ind_const) + 1;
            else
                P = matching_grampa(A, B, 0.2);
                fix_pt_ratio = sum(dot(P_rnd, P)) / n;
                robust_rate(ind_dim, ind_const) = robust_rate(ind_dim, ind_const) + (fix_pt_ratio > 0.99999);
            end
        end
    end
    toc;
    save('.\mat_files\comparison_sp_dp.mat');
end

deg_pro_rate = deg_pro_rate/num_run;
robust_rate = robust_rate/num_run;
save('.\mat_files\comparison_sp_dp.mat');